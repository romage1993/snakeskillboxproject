// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameSBGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMESB_API ASnakeGameSBGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
